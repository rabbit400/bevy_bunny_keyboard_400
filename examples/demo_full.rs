use bevy::input::common_conditions::input_toggle_active;
use bevy::prelude::*;
use bevy_bunny_textelitext_400::FontWithTextureArray;
use bevy_bunny_textelitext_400::TextMaterial;
use bevy_bunny_textelitext_400::TextMaterialPlugin;
use bevy_inspector_egui::quick::WorldInspectorPlugin;
use bevy_bunny_keyboard_400::BunnyChordsVisualizationBundle;
use bevy_bunny_keyboard_400::BunnyCurrentMenuVisualizationBundle;
use bevy_bunny_keyboard_400::BunnyKeyboardBundle;
use bevy_bunny_keyboard_400::BunnykeysPlugin;
use bevy_bunny_keyboard_400::BunnyMenusVisualizationBundle;
use bevy_bunny_keyboard_400::util::Finger;
use bevy_bunny_keyboard_400::events::KeyboardEmission;
use bevy_bunny_keyboard_400::events::SpecialEmissions;
use leafwing_input_manager::InputManagerBundle;
use leafwing_input_manager::prelude::ActionState;
use leafwing_input_manager::prelude::InputManagerPlugin;

fn main() {
    println!("Hello, world!");
    App::new()
        .add_plugins(DefaultPlugins)
        .add_plugins(WorldInspectorPlugin::default()
            .run_if(input_toggle_active(false, KeyCode::Escape)))
        .add_plugins(InputManagerPlugin::<Finger>::default())
        .add_plugins(MaterialPlugin::<TextMaterial>::default())
        .add_plugins(TextMaterialPlugin)
        .add_plugins(BunnykeysPlugin)
        
        .add_systems(PreStartup, insert_resources)
        
        .add_systems(Startup, spawn_camera_and_light)
        .add_systems(Startup, spawn_keyboard_and_visuals)
        .add_systems(Startup, spawn_characters_pressed_visualizations)
        .add_systems(Startup, spawn_quad_and_rotating_cube_to_show_any_text_written)
        
        .add_systems(Update, rotate_system)
        .add_systems(Update, update_chars_pressed_visuals)
        .add_systems(Update, move_keyboard_root)
        .add_systems(Update, update_all_set_mytext_to_keyboard_emmision)
        .run();
}

/// Spawns all stuff from the actual crate.
/// This is the only code you should really care to read in this example.
fn spawn_keyboard_and_visuals(
    mut commands: Commands,
    resources: Res<Resources>,
    mut text_materials: ResMut<Assets<TextMaterial>>,
    mut meshes: ResMut<Assets<Mesh>>,
) {
    // The code in the 'create_decent_default...' functions is what to
    // copy-paste and modify if you want more customized versions of these
    // component-bundles. It should make perfect sense to (like we do here)
    // just spawn the selected decent_default stuff you as children of a 
    // root and use the root for scaling and moving keyboard + visuals.

    // NOTE THAT SCALE is set to 0.5 on the keyboard root object in insert_resources.
    // All these spawnees scale together as children of that root.

    let font_with_textures = resources.font_with_textures.clone();

    let mut keyboard_entity : Option<Entity> = None;
    commands.entity(resources.keyboard_root).with_children(|commands|{
        let entity_id = commands.spawn((
            Name::from("keyboard"),
            BunnyKeyboardBundle::create_decent_default_keyboard_bundle(
                font_with_textures.clone(),
                &mut text_materials,
                &mut meshes,
            )
        )).id();
        keyboard_entity = Some(entity_id);
    });

    commands.entity(resources.keyboard_root).with_children(|commands|{
        commands.spawn((
            Name::from("chords visual"),
            BunnyChordsVisualizationBundle::
                create_decent_default_visualization_bundle(
                    keyboard_entity,
                    font_with_textures.clone(),
                    &mut text_materials,
                    &mut meshes
                ),
        ));
    });

    commands.entity(resources.keyboard_root).with_children(|commands|{
        commands.spawn((
            Name::from("menus visual"),
            BunnyMenusVisualizationBundle::
                create_decent_default_visualization_bundle(
                    keyboard_entity,
                    font_with_textures.clone(),
                    &mut text_materials,
                    &mut meshes
                ),
        ));
    });

    commands.entity(resources.keyboard_root).with_children(|commands|{
        commands.spawn((
            Name::from("current menu visual"),
            BunnyCurrentMenuVisualizationBundle::
                create_decent_default_visualization_bundle(
                    keyboard_entity,
                    font_with_textures,
                    &mut text_materials,
                    &mut meshes
                ),
        ));
    });
}

#[derive(Resource, Debug)]
struct Resources {
    quad: Handle<Mesh>,
    font_with_textures: Handle<FontWithTextureArray>,
    keyboard_root: Entity,
    demo_root: Entity,
}

/// Mark the root of crate entities (keyboard and visualizations).
/// This is purely so we can move it around a little bit with sin(time).
#[derive(Component)]
struct KeyboardRoot;

/// Marks entities that are to be rotated. This is only the cube with text on sides.
#[derive(Component)]
struct RotateThis {
    axis: Vec3,
    speed: f32,
}

/// This is used with a text-material to show which keys are pressed to form a chord.
/// This is not important and naturally the spawn and update systems are a mess.
#[derive(Component)]
struct CharsPressedVisualisation{ chars: Vec<(char, Finger)> }

#[derive(Component)]
struct SetMytextToKeyboardEmmision {
    text: Vec<char>,
    row_length: u32,
    num_rows: u32,
    next_index: usize,
}
impl SetMytextToKeyboardEmmision {
    fn step_back(self: &mut Self) {
        self.next_index += (self.row_length * self.num_rows) as usize -1;
    }
    fn step_forward(self: &mut Self) {
        self.next_index +=1;
    }
    fn add_char(self: &mut Self, c: char) {
        if self.text.len() < (self.row_length * self.num_rows) as usize {
            self.text.push(c);
            self.next_index = self.text.len();
        }
        else if  self.text.len() == 0 {
        } // do nothing
        else {
            let i = self.next_index % self.text.len();
            self.text[i] = c;
            self.next_index = i + 1;
        }
    }
}
impl Default for SetMytextToKeyboardEmmision {
    fn default() -> Self { Self {
            text: "ABCDefghIJKLmnop".chars().collect(),
            row_length: 4,
            num_rows: 4,
            next_index: 0,
} } }

fn insert_resources(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut font_w_textures_assets: ResMut<Assets<FontWithTextureArray>>,
) {
    // We set scale to 0.5 because thats a good size for us.
    // This will scale all children.
    // I have changed default size for keyboard since I first wrote this example...
    // I am to lazy to redo this examples layout AND ALSO this is a good illustration
    // of how to scale and move keyboard with extras together as children.
    let keyboard_root = commands.spawn((
        Name::from("keyboard_root"),
        KeyboardRoot,
        SpatialBundle {
            transform: Transform::from_scale(0.5 * Vec3::ONE),
            ..default()
        }
    )).id();

    let demo_root = commands.spawn((
        Name::from("demo_root"),
        SpatialBundle {
            transform: Transform::from_translation(1.0 * Vec3::Y),
            ..default()
        },
    )).id();

    let quad = meshes.add(Mesh::from(shape::Quad::default()));

    commands.insert_resource(Resources{
        font_with_textures: font_w_textures_assets.add(
            FontWithTextureArray::new(100, 100, false, None, None, None, Some(' ')),
        ),
        keyboard_root,
        demo_root,
        quad,
    });
}

fn spawn_camera_and_light(
    mut commands: Commands,
) {
    // it will look at (0,0,0) using up = Y = (0,1,0)
    let camera_position = Vec3::new(1.0, 1.0, 7.0);

    commands.spawn((
        Name::from("Camera"),
        Camera3dBundle {
            transform: Transform::default()
                .with_translation(camera_position)
                .looking_at(Vec3::ZERO, Vec3::Y),
            ..default()
        },
    ));

    commands.spawn((
        Name::from("DirectionalLight"),
        DirectionalLightBundle {
            transform: Transform::default()
                .with_translation(camera_position)
                .looking_at(Vec3::ZERO, Vec3::Y),
            ..default()
        },
    ));
}

fn spawn_characters_pressed_visualizations(
    mut commands: Commands,
    resources: Res<Resources>,
    mut text_mats: ResMut<Assets<TextMaterial>>,
) {
    let mut spawn_that = |
        chars_vis: Vec<(char, Finger)>,
        commands: &mut ChildBuilder,
        transform: Transform,
    | {
        let chars: Vec<char> = chars_vis.iter().map(|(c, _)| *c).collect();
        let name: String = String::from_iter(chars.iter());
        let scale = 0.2;
        let scales = Vec3::new(scale * (chars.len() as f32), scale, 0.0);
        let transform = transform.with_scale(scales);

        let mut material = TextMaterial::new(resources.font_with_textures.clone());
        material.set_text(chars.clone(), chars.len().try_into().unwrap(), 1);

        commands.spawn((
            Name::new(name),
            CharsPressedVisualisation{ chars: chars_vis },
            MaterialMeshBundle {
                material: text_mats.add(material),
                mesh: resources.quad.clone(),
                transform,
                ..default()
            },
            InputManagerBundle {
                input_map: BunnyKeyboardBundle::create_good_default_input_map(),
                ..default()
            }
        ));
    };

    commands.entity(resources.demo_root)
    .with_children(|mut commands|
    {
        spawn_that(vec![
                ('A', Finger::LPinkie),
                ('S', Finger::LRing),
                ('D', Finger::LMiddle),
                ('F', Finger::LIndex),
            ],
            &mut commands,
            Transform::from_translation(Vec3::new(-1.0, 0.0, 0.0))
        );
        spawn_that(vec![
                ('H', Finger::RIndex),
                ('J', Finger::RMiddle),
                ('K', Finger::RRing),
                ('L', Finger::RPinkie),
            ],
            &mut commands,
            Transform::from_translation(Vec3::new(1.0, 0.0, 0.0))
        );
    });
}

fn spawn_quad_and_rotating_cube_to_show_any_text_written(
    mut commands: Commands,
    mut text_materials: ResMut<Assets<TextMaterial>>,
    mut mesh_assets: ResMut<Assets<Mesh>>,
    resources: Res<Resources>,
) {
    let quad_transform = Transform::default()
        .with_translation(Vec3::X + Vec3::Y);
    let cube_transform = Transform::default()
        .with_translation(-Vec3::X + Vec3::Y)
        .with_scale(0.5 * Vec3::ONE);

    let mesh = Mesh::from(shape::Cube{ ..default() });
    let mut quad_material = TextMaterial::new(resources.font_with_textures.clone());
    quad_material.set_text("ABCDefghIJLKmnop".chars().collect(), 4, 4);
    
    // We need to clone the material so cube and quad can have different texts.
    // (They actually have the same text but using the same material would make
    //  the update logic different and a worse example.)
    let cube_material = quad_material.clone();

    commands.entity(resources.demo_root).with_children(|commands| {
        commands.spawn((
            Name::from("Quad"),
            SetMytextToKeyboardEmmision::default(),
            MaterialMeshBundle{
                material: text_materials.add(quad_material),
                mesh: mesh_assets.add(Mesh::from(shape::Quad{ ..default() })),
                transform: quad_transform,
                ..default()
            }
        ));
        commands.spawn((
            Name::from("Cube"),
            RotateThis { axis: Vec3::ONE.normalize(), speed: 1.0 },
            SetMytextToKeyboardEmmision::default(),
            MaterialMeshBundle{
                material: text_materials.add(cube_material),
                mesh: mesh_assets.add(mesh.clone()),
                transform: cube_transform,
                ..default()
            }
        ));
    });

}

fn move_keyboard_root(mut roots: Query<&mut Transform, With<KeyboardRoot>>, time: Res<Time>)
{
    for mut root in roots.iter_mut() {
        root.translation = Vec3::new(0.0, 0.2 * f32::sin(time.elapsed_seconds()), 0.0);
    }
}

fn rotate_system(mut q: Query<(&RotateThis, &mut Transform)>)
{
    let dt = 0.01;
    for (r, mut t) in q.iter_mut() {
        t.rotate_axis(r.axis, r.speed * dt);
    }
}

fn update_chars_pressed_visuals(
    mut query: Query<(
        &CharsPressedVisualisation,
        &Handle<TextMaterial>,
        &ActionState<Finger>
    )>,
    mut text_materials: ResMut<Assets<TextMaterial>>,
) {
    for (cv, material_handle, action) in query.iter_mut()
    {
        let is_pressed = |finger| {
            action.pressed(finger) || action.just_pressed(finger)
        };
        let any_finger_pressed
            = cv.chars.iter().any(|(_, f)| is_pressed(*f));
        
        let m = text_materials.get_mut(material_handle).unwrap();

        let text;
        let text_colour;

        if any_finger_pressed {
            text_colour = Color::WHITE;
            text = cv.chars.iter().map(|(c, f)|
                    if is_pressed(*f) { *c } else { ' ' }
                ).collect();
        } else {
            text_colour = Color::GRAY;
            text = cv.chars.iter().map(|(c, _)| *c).collect();
        }

        m.set_text(text, cv.chars.len() as u32, 1);
        m.set_text_colour(text_colour);
    }
}

fn update_all_set_mytext_to_keyboard_emmision(
    mut emmisions: EventReader<KeyboardEmission>,
    mut text_materials: ResMut<Assets<TextMaterial>>,
    mut query: Query<(&Handle<TextMaterial>, &mut SetMytextToKeyboardEmmision)>,
) {
    if let Some(last_event) = emmisions.read().last() {
        for (mat_handle, mut text_info) in query.iter_mut() {
            if let Some(m) =  text_materials.get_mut(mat_handle) {
                match last_event {
                    KeyboardEmission::Character(c) => {
                        text_info.add_char(*c);
                    }
                    KeyboardEmission::Special(val) => {
                        use SpecialEmissions as se;
                        match val {
                            se::Enter => {
                                text_info.add_char(' ');
                                while 0 != text_info.next_index % text_info.row_length as usize {
                                    text_info.add_char(' ');
                                }
                            }
                            se::Backspace => { text_info.step_back(); },
                            se::Delete => { text_info.add_char('_'); }
                            se::Home => {
                                while 0 != text_info.next_index % text_info.row_length as usize {
                                    text_info.step_back();
                                }
                            }
                            se::End => {
                                while 0 != (1 + text_info.next_index) % text_info.row_length as usize {
                                    text_info.step_forward();
                                }
                            }
                            _ => {
                                let s = format!("{:?}", val);
                                for c in s.chars() {
                                    text_info.add_char(c);
                                }
                            }
                        }                   
                        text_info.add_char('_');
                        text_info.step_back();
                    }
                }
                m.set_text(text_info.text.clone(), text_info.row_length, text_info.num_rows);
            }
        }
    }
}
