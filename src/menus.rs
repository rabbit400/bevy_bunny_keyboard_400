
use crate::events::*;

pub trait MenuTrait : Clone {
    fn characters(self: &Self, order: Option<u8>) -> Vec<char>;
    fn emission(self: &Self, order: Option<u8>) -> Option<KeyboardEmission>;
}

#[derive(Clone)]
pub enum Menu {
    EmptyMenu(EmptyMenu),
    CharactersMenu(CharactersMenu),
    SpecialMenu(SpecialMenu),
}
impl From<EmptyMenu> for Menu {
    fn from(value: EmptyMenu) -> Self {Self::EmptyMenu(value)}
}
impl From<SpecialMenu> for Menu {
    fn from(value: SpecialMenu) -> Self {Self::SpecialMenu(value)}
}
impl From<CharactersMenu> for Menu {
    fn from(value: CharactersMenu) -> Self {Self::CharactersMenu(value)}
}
impl MenuTrait for Menu {
    fn characters(self: &Self, order: Option<u8>) -> Vec<char> {
        match self {
            Menu::EmptyMenu(m) => m.characters(order),
            Menu::CharactersMenu(m) => m.characters(order),
            Menu::SpecialMenu(m) => m.characters(order),
        }
    }
    fn emission(self: &Self, order: Option<u8>) -> Option<KeyboardEmission> {
        match self {
            Menu::EmptyMenu(m) => m.emission(order),
            Menu::CharactersMenu(m) => m.emission(order),
            Menu::SpecialMenu(m) => m.emission(order),
        }
    }
}

#[derive(Clone)]
pub struct EmptyMenu {
    name: Vec<char>,
}
impl EmptyMenu {
    pub fn new(name_str: &str) -> Self { Self { name: name_str.chars().collect() } }
}
impl MenuTrait for EmptyMenu {
    fn characters(self: &Self, _order: Option<u8>) -> Vec<char> { self.name.clone() }
    fn emission(self: &Self, _order: Option<u8>) -> Option<KeyboardEmission> { None }
}

#[derive(Clone)]
pub struct CharactersMenu { characters: Vec<char> }
impl CharactersMenu {
    pub fn new(characters: &str) -> Self { Self {
        characters: characters.chars().collect()
    } }
}
impl MenuTrait for CharactersMenu {
    fn characters(self: &Self, order: Option<u8>) -> Vec<char> {
        match order {
            None => self.characters.clone(),
            Some(o) => {
                let mut ret = vec![' '; self.characters.len()];
                let i = o as usize;
                if i < ret.len() {
                    ret[i] = self.characters[i];
                }
                ret
            },
        }
    }
    fn emission(self: &Self, order: Option<u8>) -> Option<KeyboardEmission> {
        if let Some(o) = order {
            if let Some(character) = self.characters.get(o as usize) {
                return Some(KeyboardEmission::Character(*character));
            }
        }
        None
    }
}

#[derive(Clone)]
pub struct SpecialMenu {
    pub name: Vec<char>,
    pub name_for_no_option: Vec<char>,
    pub items: Vec<SpecialMenuOption>,
}
impl MenuTrait for SpecialMenu {
    fn characters(self: &Self, order: Option<u8>) -> Vec<char> {
        if let Some(o) = order {
            if let Some(item) = self.items.get(o as usize) {
                return [self.name.as_slice(), item.name.as_slice()].concat();
            }
        }
        return [self.name.as_slice(), self.name_for_no_option.as_slice()].concat();
    }
    fn emission(self: &Self, order: Option<u8>) -> Option<KeyboardEmission> {
        if let Some(o) = order {
            if let Some(item) = self.items.get(o as usize) {
                return item.emission.clone();
            }
        }
        None
    }
}
#[derive(Clone)]
pub struct SpecialMenuOption {
    name: Vec<char>,
    emission: Option<KeyboardEmission>,
}
impl SpecialMenuOption {
    pub fn new(str: &str, emission: Option<KeyboardEmission>) -> Self
    { Self {
        name: str.chars().collect(),
        emission,
    } }
}
