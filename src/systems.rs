use bevy::prelude::*;
use bevy_bunny_textelitext_400::TextMaterial;
use leafwing_input_manager::prelude::ActionState;

use crate::components::*;
use crate::chord::*;
use crate::events::*;
use crate::menus::*;
use crate::util::*;

pub fn update_keyboard(
    mut q: Query<(&Handle<TextMaterial>, &mut Keyboard, &ActionState<Finger>)>,
    mut text_mats: ResMut<Assets<TextMaterial>>,
    mut emittor: EventWriter<KeyboardEmission>,
    time: Res<Time>,
) {
    for (mat_handle, mut keyboard, acts) in q.iter_mut()
    {
        let full_chord = read_full_chord(acts);

        let report_result = keyboard.memory
            .report(full_chord, time.delta_seconds());

        if let Some(completed_full_chord) = report_result.completed_chord
        {
            if let Some(menu) = keyboard.menus.get(&completed_full_chord.left_chord)
            {
                let order = keyboard.right_chord_order(completed_full_chord.right_chord);
                if let Some(emission) = menu.emission(order) {
                    emittor.send(emission);
                }
            }
        }

        let menu = keyboard.menus.get(&full_chord.left_chord);
        let order = keyboard.right_chord_order(full_chord.right_chord);
        let text_material = text_mats.get_mut(mat_handle).unwrap();

        let current_chord_would_emit_if_released
            =  report_result.current_chord_past_completion_time
            && menu.is_some_and(|m| {
                m.emission(order).is_some()
            });

        let text_colour = if current_chord_would_emit_if_released
        { Color::WHITE }
        else
        { Color::BLACK };

        text_material.set_text_colour(text_colour);

        if full_chord != keyboard.last_chord {
            let text: Vec<char> = match menu {
                Some(t)
                    => t.characters(order),
                None
                    => "  no menu   ".chars().collect(),
            };
            keyboard.last_chord = full_chord;
            
            let len = text.len() as u32;
            text_material.set_text(text, len, 1);
        }
    }
}

pub fn update_chords_visualizations(
    mut q: Query<(&Handle<TextMaterial>, &mut ChordsVisualization)>,
    keyboards: Query<&Keyboard>,
    mut text_mats: ResMut<Assets<TextMaterial>>,
) {
    for (mat_handle, mut vis) in q.iter_mut()
    {
        let Some(keyboard_entity) = vis.keyboard
            else { continue; };
        let Ok(keyboard) = keyboards.get(keyboard_entity)
            else { continue; };
        let Some(text_material) = text_mats.get_mut(mat_handle)
            else { continue; };

        let chord = keyboard.last_chord.side(vis.side);
        if chord != vis.last_chord
        {
            vis.last_chord = chord;

            let chords = match vis.side {
                ChordSide::Left => {
                    let mut chords: Vec<Chord> =
                    keyboard.menus.iter().map(|(c, _)| *c).collect();
                    chords.sort();
                    chords
                },
                ChordSide::Right => {
                    let chords = Chord::all_chords().iter().cloned()
                        .filter(|c| keyboard.right_chord_order(*c).is_some())
                        .collect();
                    chords
                },
            };

            let mut text = Vec::<char>::with_capacity(chords.len() * 5);
            
            for c in chords {
                let show_chord_bold = c == chord;
                let chord_text = c.display(vis.side, show_chord_bold);
                text.extend_from_slice(&chord_text);
                text.push(' ');
            }
            let n = text.len() as u32;
            text_material.set_text(text, n, 1);
        }
    }
}

pub fn update_menus_visualizations(
    mut q: Query<(&Handle<TextMaterial>, &mut MenusVisualization)>,
    keyboard_query: Query<&Keyboard>,
    mut text_mats: ResMut<Assets<TextMaterial>>,
){
    for (text_handle, mut menus_viz) in q.iter_mut()
    {
        let Some(keyboard_entity) = menus_viz.keyboard
            else { continue; };
        let Ok(keyboard) = keyboard_query.get(keyboard_entity)
            else { continue; };

        let chord = keyboard.last_chord.left_chord;
    
        if chord != menus_viz.last_chord
        {
            if let Some(text_mat) = text_mats.get_mut(text_handle)
            {
                menus_viz.last_chord = chord;

                let mut menus: Vec<(&Chord, &Menu)> =
                    keyboard.menus.iter().collect();
                menus.sort_by_key(|(c, _m)| *c);

                let num_rows = menus.len();
                let row_length = menus.iter()
                    .map(|(_c, m)| m.characters(None).len())
                    .max().unwrap_or(0)
                    + 5;
    
                let mut text = Vec::<char>::with_capacity(row_length * num_rows);
                
                for (c, m) in menus.iter().cloned()
                {
                    let bold = *c == chord;
                    let l_fill = if bold { '>' } else { ' ' };
                    let r_fill = if bold { '<' } else { ' ' };
                    let chord_vis = c.display(ChordSide::Left, bold);

                    let mut chars = m.characters(None);
                    let num_extra_chars_to_right = row_length - 5 - chars.len();

                    text.append(&mut chars);
                    text.push(l_fill);
                    text.extend_from_slice(&chord_vis);

                    let new_size = text.len() + num_extra_chars_to_right;
                    text.resize(new_size, r_fill);
                }
                text_mat.set_text(text, row_length as u32, num_rows as u32)
            }
        }
    }
}

pub fn update_current_menu_visualizations(
    mut q: Query<(&Handle<TextMaterial>, &mut CurrentMenuVisualization)>,
    keyboard_query: Query<&Keyboard>,
    mut text_mats: ResMut<Assets<TextMaterial>>,
) {
    for (text_mat_handle, mut vis) in q.iter_mut()
    {
        let Some(keyboard_entity) = vis.keyboard
            else { continue; };
        let Ok(keyboard) = keyboard_query.get(keyboard_entity)
            else { continue; };

        if vis.last_chord == keyboard.last_chord
            { continue; }
        vis.last_chord = keyboard.last_chord;

        let Some(text_mat) = text_mats.get_mut(text_mat_handle)
            else { continue; };

        let Some(menu) = keyboard.menus.get(&keyboard.last_chord.left_chord)
        else {
            text_mat.set_text(Vec::from([' ']), 1, 1);
            continue;
        };

        let get_row = |(chord, order): (Chord, Option<u8>)| {
            let bold = chord == keyboard.last_chord.right_chord;
            let chord_chars = chord.display(ChordSide::Right, bold);
            let mut chars = menu.characters(order);
            let mut ret = Vec::<char>::with_capacity(5 + chars.len());
            ret.extend_from_slice(&chord_chars);
            ret.push(' ');
            ret.append(&mut chars);
            ret
        };

        let all_rows = Chord::all_chords().iter().cloned()
            .map(|c| (c, keyboard.right_chord_order(c)))
            .filter(|(_c, o)| o.is_some())
            .map(|tuple| get_row(tuple))
            .collect::<Vec<Vec<char>>>();

        let num_rows = all_rows.len();
        let Some(row_len) = all_rows.iter().map(|row| row.len()).max()
        else {
            text_mat.set_text(Vec::from([' ']), 1, 1);
            continue;
        };

        let mut ret = Vec::<char>::with_capacity(row_len * all_rows.len());

        for row in all_rows.iter() {
            let num_extra_chars = row_len - row.len();
            ret.extend_from_slice(row);
            ret.resize(ret.len() + num_extra_chars, ' ');
        }

        text_mat.set_text(ret, row_len as u32, num_rows as u32);
    }
}
