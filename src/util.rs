use bevy::reflect::Reflect;
use leafwing_input_manager::prelude::ActionState;
use leafwing_input_manager::Actionlike;

use crate::chord::*;


#[derive(Actionlike, Clone, Debug, Copy, PartialEq, Eq, Reflect)]
pub enum Finger {
    LIndex,
    LMiddle,
    LRing,
    LPinkie,
    RIndex,
    RMiddle,
    RRing,
    RPinkie,
}
pub fn read_full_chord(acts: &ActionState<Finger>) -> FullChord {
    let full_chord = {
        let pressed = |f| acts.just_pressed(f) || acts.pressed(f);

        let left_chord = Chord::new(
            pressed(Finger::LIndex),
            pressed(Finger::LMiddle),
            pressed(Finger::LRing),
            pressed(Finger::LPinkie),
        );
        let right_chord = Chord::new(
            pressed(Finger::RIndex),
            pressed(Finger::RMiddle),
            pressed(Finger::RRing),
            pressed(Finger::RPinkie),
        );
        FullChord{ left_chord, right_chord }
    };
    full_chord
}
