use bevy::prelude::*;

#[derive(Event, Clone, Debug)]
pub enum KeyboardEmission {
    Character(char),
    Special(SpecialEmissions),
}
impl From<SpecialEmissions> for KeyboardEmission{
    fn from(value: SpecialEmissions) -> Self {
        Self::Special(value)
    }
}

#[derive(Clone, Debug)]
pub enum SpecialEmissions {
    Enter,
    Backspace,
    Delete,
    Home,
    End,
    PageUp,
    PageDown
}
