use bevy::prelude::*;
use bevy_bunny_textelitext_400::FontWithTextureArray;
use bevy_bunny_textelitext_400::TextMaterial;
use leafwing_input_manager::InputManagerBundle;
use leafwing_input_manager::prelude::InputMap;

pub mod components;
pub mod chord;
pub mod events;
pub mod menus;
pub mod systems;
pub mod util;

use components::*;
use events::*;
use systems::*;
use util::*;

pub struct BunnykeysPlugin;

impl Plugin for BunnykeysPlugin {
    fn build(&self, app: &mut App) {
        app
            .add_event::<KeyboardEmission>()

            .add_systems(Update, update_keyboard)
            .add_systems(Update, update_chords_visualizations)
            .add_systems(Update, update_menus_visualizations
                                    .after(update_keyboard))
            .add_systems(Update, update_current_menu_visualizations
                                    .after(update_keyboard))
            ;
    }
}

#[derive(Bundle)]
pub struct BunnyKeyboardBundle {
    pub keyboard: Keyboard,
    pub input_manager_bundle: InputManagerBundle<Finger>,
    pub material_mesh_bundle: MaterialMeshBundle<TextMaterial>,
}
impl Default for BunnyKeyboardBundle {
    fn default() -> Self { Self {
        keyboard: Default::default(),
        input_manager_bundle: InputManagerBundle
        {
            input_map: Self::create_good_default_input_map(),
            ..default()
        },
        material_mesh_bundle: Default::default(),
    } }
}
impl BunnyKeyboardBundle {
    pub fn create_decent_default_keyboard_bundle(
        font_with_textures: Handle<FontWithTextureArray>,
        text_materials: &mut ResMut<Assets<TextMaterial>>,
        meshes: &mut ResMut<Assets<Mesh>>,
    )
        -> BunnyKeyboardBundle
    {
        let quad_mesh = Mesh::from(shape::Quad::default());
        let transform = decent_default_layout::keyboard::transform();
    
        let mut material = TextMaterial::new(font_with_textures);
        // make it show something helpful before the update system runs
        material.set_text("Keyboard init".chars().collect(), 12, 1);
    
        BunnyKeyboardBundle {
            material_mesh_bundle: MaterialMeshBundle{
                mesh:       meshes.add(quad_mesh),
                material:   text_materials.add(material),
                transform,
                ..default()
            },
            ..default()
        }
    }
    pub fn create_good_default_input_map() -> InputMap<Finger> {
        InputMap::<Finger>::default()
            .insert(KeyCode::F, Finger::LIndex)
            .insert(KeyCode::D, Finger::LMiddle)
            .insert(KeyCode::S, Finger::LRing)
            .insert(KeyCode::A, Finger::LPinkie)
            .insert(KeyCode::H, Finger::RIndex)
            .insert(KeyCode::J, Finger::RMiddle)
            .insert(KeyCode::K, Finger::RRing)
            .insert(KeyCode::L, Finger::RPinkie)
            .build()
    }
}

#[derive(Bundle)]
pub struct BunnyChordsVisualizationBundle {
    pub chords_visuals: ChordsVisualization,
    pub material_mesh_bundle: MaterialMeshBundle<TextMaterial>,
}
impl Default for BunnyChordsVisualizationBundle {
    fn default() -> Self { Self {
        chords_visuals: Default::default(),
        material_mesh_bundle: Default::default(),
    } }
}
impl BunnyChordsVisualizationBundle {
    pub fn create_decent_default_visualization_bundle(
        keyboard: Option<Entity>,
        font_with_textures: Handle<FontWithTextureArray>,
        text_materials: &mut ResMut<Assets<TextMaterial>>,
        meshes: &mut ResMut<Assets<Mesh>>,
    )
        -> Self
    {
        let quad_mesh = Mesh::from(shape::Quad::default());
        let transform = decent_default_layout::chords_visualization::transform();
    
        let mut material = TextMaterial::new(font_with_textures);
        // make it show something helpful before the update system runs
        material.set_text("chord visuals init".chars().collect(), 20, 1);
    
        BunnyChordsVisualizationBundle {
            chords_visuals: ChordsVisualization {
                keyboard,
                ..default()
            },
            material_mesh_bundle: MaterialMeshBundle{
                mesh:       meshes.add(quad_mesh),
                material:   text_materials.add(material),
                transform,
                ..default()
            },
            ..default()
        }
    }
}

#[derive(Bundle)]
pub struct  BunnyMenusVisualizationBundle {
    pub menus_visuals: MenusVisualization,
    pub material_mesh_bundle: MaterialMeshBundle<TextMaterial>,
}
impl Default for BunnyMenusVisualizationBundle {
    fn default() -> Self { Self {
        menus_visuals: Default::default(),
        material_mesh_bundle: Default::default(),
    } }
}
impl BunnyMenusVisualizationBundle {
    pub fn create_decent_default_visualization_bundle(
        keyboard: Option<Entity>,
        font_with_textures: Handle<FontWithTextureArray>,
        text_materials: &mut ResMut<Assets<TextMaterial>>,
        meshes: &mut ResMut<Assets<Mesh>>,
    )
        -> Self
    {
        let quad_mesh = Mesh::from(shape::Quad::default());
        let transform = decent_default_layout::menus_visualization::transform();
    
        let mut material = TextMaterial::new(font_with_textures);
        // make it show something helpful before the update system runs
        material.set_text("menus visuals init".chars().collect(), 10, 10);
    
        BunnyMenusVisualizationBundle {
            material_mesh_bundle: MaterialMeshBundle{
                mesh:       meshes.add(quad_mesh),
                material:   text_materials.add(material),
                transform,
                ..default()
            },
            menus_visuals: MenusVisualization {
                keyboard,
                ..default()
            },
            ..default()
        }
    }
}

#[derive(Bundle)]
pub struct  BunnyCurrentMenuVisualizationBundle {
    pub menus_visuals: CurrentMenuVisualization,
    pub material_mesh_bundle: MaterialMeshBundle<TextMaterial>,
}
impl Default for BunnyCurrentMenuVisualizationBundle {
    fn default() -> Self { Self {
        menus_visuals: Default::default(),
        material_mesh_bundle: Default::default(),
    } }
}
impl BunnyCurrentMenuVisualizationBundle {
    pub fn create_decent_default_visualization_bundle(
        keyboard: Option<Entity>,
        font_with_textures: Handle<FontWithTextureArray>,
        text_materials: &mut ResMut<Assets<TextMaterial>>,
        meshes: &mut ResMut<Assets<Mesh>>,
    )
        -> Self
    {
        let quad_mesh = Mesh::from(shape::Quad::default());
        let transform = decent_default_layout::current_menu_visualizations::transform();
    
        let mut material = TextMaterial::new(font_with_textures);
        // make it show something helpful before the update system runs
        material.set_text("current menu visuals init".chars().collect(), 10, 10);
    
        BunnyCurrentMenuVisualizationBundle {
            material_mesh_bundle: MaterialMeshBundle{
                mesh:       meshes.add(quad_mesh),
                material:   text_materials.add(material),
                transform,
                ..default()
            },
            menus_visuals: CurrentMenuVisualization{
                keyboard,
                ..default()
            },
            ..default()
        }
    }
}

mod decent_default_layout
{
    const SPACING: f32 = 0.05;

    pub mod keyboard
    {
        use bevy::prelude::*;

        // we know we will want 12 characters shown
        // this makes them all square with side 1.0... good enough
        pub const WIDTH: f32 = 12.0;
        pub const HEIGHT: f32 = 1.0;

        pub fn transform() -> Transform {
            Transform::default()
                .with_scale(Vec3::new(WIDTH, HEIGHT, 1.0))
        }
    }

    pub mod chords_visualization
    {
        use bevy::prelude::*;
        use super::*;

        // we assume we want to fit above a default keyboard
        const SCALE_X: f32 = keyboard::WIDTH;
        // make our characters height squashed - these are chord visualizations
        const SCALE_Y: f32 = 0.5 * keyboard::HEIGHT;
        const SCALE: Vec3 = Vec3::new(SCALE_X, SCALE_Y, 1.0);

        const TRANSLATION_Y: f32 = 0.5 * SCALE_Y + 0.5 * keyboard::HEIGHT + SPACING;
        const TRANSLATION: Vec3 = Vec3::new(0.0, TRANSLATION_Y, 0.0);
        
        pub fn transform() -> Transform {
            Transform::default()
                .with_scale(SCALE).with_translation(TRANSLATION)
        }
    }

    pub mod menus_visualization
    {
        use bevy::prelude::*;
        use super::*;

        const ROW_LENGTH: u32 = 17;
        const NUM_ROWS: u32 = 12;
        const LINES_PER_COLUMN: f32 = NUM_ROWS as f32 / (ROW_LENGTH as f32);

        // we assume we want to fit below the left half default keyboard
        // half of space to right comes from that component moving right
        const SCALE_X: f32 = 0.5 * keyboard::WIDTH - 0.5 * SPACING;
        // make characters height larger than width
        const SCALE_Y: f32 = 1.2 * SCALE_X * LINES_PER_COLUMN;
        const SCALE: Vec3 = Vec3::new(SCALE_X, SCALE_Y, 1.0);

        // half of space to left comes from other component moving left
        const TRANSLATION_X: f32 = -0.5 * SCALE_X - 0.5 * SPACING;
        const TRANSLATION_Y: f32 = -0.5 * SCALE_Y - 0.5 * keyboard::HEIGHT - SPACING;
        const TRANSLATION: Vec3 = Vec3::new(TRANSLATION_X, TRANSLATION_Y, 0.0);
        
        pub fn transform() -> Transform {
            Transform::default()
                .with_scale(SCALE).with_translation(TRANSLATION)
        }
    }

    pub mod current_menu_visualizations
    {
        use bevy::prelude::*;
        use super::*;

        const ROW_LENGTH: u32 = 17;
        const NUM_ROWS: u32 = 12;
        const LINES_PER_COLUMN: f32 = NUM_ROWS as f32 / (ROW_LENGTH as f32);

        // we assume we want to fit below the right half default keyboard
        // half of space to left comes from that component moving left
        const SCALE_X: f32 = 0.5 * keyboard::WIDTH - 0.5 * SPACING;
        // make characters height larger than width
        const SCALE_Y: f32 = 1.2 * SCALE_X * LINES_PER_COLUMN;
        const SCALE: Vec3 = Vec3::new(SCALE_X, SCALE_Y, 1.0);

        // half of space to left comes from other component moving left
        const TRANSLATION_X: f32 = 0.5 * SCALE_X + 0.5 * SPACING;
        const TRANSLATION_Y: f32 = -0.5 * SCALE_Y - 0.5 * keyboard::HEIGHT - SPACING;
        const TRANSLATION: Vec3 = Vec3::new(TRANSLATION_X, TRANSLATION_Y, 0.0);

        pub fn transform() -> Transform {
            Transform::default()
                .with_scale(SCALE).with_translation(TRANSLATION)
        }
    }
}

