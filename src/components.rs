use std::collections::HashMap;

use bevy::prelude::*;

use crate::chord::*;
use crate::menus::*;
use crate::events::*;

#[derive(Component)]
pub struct Keyboard {
    pub menus: HashMap<Chord, Menu>,
    pub right_chord_orders: Vec<Option<u8>>,
    pub memory: ChordMemory<FullChord>,
    pub last_chord: FullChord,
}
impl Keyboard {
    pub fn right_chord_order(self: &Self, chord: Chord) -> Option<u8> {
        self.right_chord_orders[chord.bits as usize]
    }
}
impl Default for Keyboard {
    fn default() -> Self {
        let special_menu = SpecialMenu {
            name: "spec:".chars().collect(),
            name_for_no_option: "       ".chars().collect(),
            items: vec![
                SpecialMenuOption::new("enter  ", Some(SpecialEmissions::Enter.into())),
                SpecialMenuOption::new("backspc", Some(SpecialEmissions::Backspace.into())),
                SpecialMenuOption::new("delete ", Some(SpecialEmissions::Delete.into())),
                SpecialMenuOption::new("home   ", Some(SpecialEmissions::Home.into())),
                SpecialMenuOption::new("end    ", Some(SpecialEmissions::End.into())),
                SpecialMenuOption::new("page up", Some(SpecialEmissions::PageUp.into())),
                SpecialMenuOption::new("pagedwn", Some(SpecialEmissions::PageDown.into())),
            ],
        };

        const KEY_ROWS: [&str; 8] = [
            " zxcvbnm,./`",
            "asdfghjkl;'\\",
            "qwertyuiop[]",
            "1234567890-=",
            " ZXCVBNM<>?~",
            "ASDFGHJKL:\"|",
            "QWERTYUIOP{}",
            "!@#$%^&*()_+",
        ];
        let mut menus = HashMap::<Chord, Menu>::with_capacity(15);
        
        menus.insert(Chord::NONE_PRESSED, EmptyMenu::new("do ASDF HJKL").into());

        menus.insert(Chord::new(true, false, false, true),
            Menu::SpecialMenu(special_menu));

        for (i, row) in KEY_ROWS.iter().enumerate() {
            menus.insert(
                Chord{ bits: (i + 1).try_into().unwrap()},
                CharactersMenu::new(row).into());
        }
        let right_chord_orders = get_good_default_chord_orders();

        Self {
            menus,
            right_chord_orders,
            memory: default(),
            last_chord: FullChord{ left_chord: Chord::ALL_PRESSED, right_chord: Chord::ALL_PRESSED }
        }
    }
}

#[derive(Component)]
pub struct ChordsVisualization {
    pub side: ChordSide,
    pub keyboard: Option<Entity>,
    pub last_chord: Chord,
}
impl Default for ChordsVisualization {
    fn default() -> Self { Self {
        side: ChordSide::Right,
        keyboard: None,
        last_chord: Chord::ALL_PRESSED
    } }
}

#[derive(Component, Default)]
pub struct MenusVisualization {
    pub keyboard: Option<Entity>,
    pub last_chord: Chord,
}

#[derive(Component, Default)]
pub struct CurrentMenuVisualization {
    pub keyboard: Option<Entity>,
    pub last_chord: FullChord,
}

