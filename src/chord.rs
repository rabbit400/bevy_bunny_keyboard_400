use bevy::prelude::*;

/// Trait for a chord type that can be 'released'.
/// Think of released as 'no keys pressed' or 'no fingers curled'.
/// Though at time of writing 'release' for a full hand chord means 
/// 'no right hand fingers curled/pressed' so this concept could 
/// use some rethinking/renaming :D
/// Maybe 'is completing chord' or 'is emitting chord' as in this/these
/// chord[s] means that any completed chord should be emitted?
pub trait ChordWithRelease
{
    fn is_released(self: &Self) -> bool;
}

/// This contains all logic for remembering what chord is currently held
/// and for deciding when and what chord is emitted. Note that this struct
/// with member functions is completely generic on type of chord. So 
/// hopefully ChordMemory should work fine with many types of input.
/// 
/// At time of writing the default logic settings and types of chords used
/// are for using valve index 'knuckle' controllers fingers sensors for 
/// pinkie, ring, middle and index fingers - ignoring thumbs.
pub struct ChordMemory<TChord>
where TChord: ChordWithRelease + Default + PartialEq + Clone
{
    seconds_to_complete: f32,
    seconds_to_forget_completed: f32,
    last_completed_chord: TChord,
    seconds_since_completion: f32,
    current_chord: TChord,
    seconds_of_current_chord: f32,
}
impl<TChord> Default for ChordMemory<TChord>
where TChord: ChordWithRelease + Default + PartialEq + Clone
{
    fn default() -> Self { Self {
        seconds_to_complete: 0.2,
        seconds_to_forget_completed: 0.1,
        last_completed_chord: default(),
        seconds_since_completion: f32::INFINITY,
        current_chord: default(),
        seconds_of_current_chord: 0.0 }
    }
}
impl<TChord> ChordMemory<TChord>
where TChord: ChordWithRelease + Default + PartialEq + Clone
{
    pub fn report(self: &mut Self, chord: TChord, seconds_elapsed: f32) -> ChordMemoryReport<TChord>
    {
        if chord == self.current_chord {
            self.seconds_of_current_chord += seconds_elapsed;
            self.seconds_since_completion += seconds_elapsed;
            return self.create_report(None);
        }

        let mut estimated_seconds_since_completion
            = self.seconds_since_completion + 0.5 * seconds_elapsed;
        let estimated_seconds_in_current_chord
            = self.seconds_of_current_chord + 0.5 * seconds_elapsed;

        // update completed chord data
        if estimated_seconds_in_current_chord >= self.seconds_to_complete
        {
            estimated_seconds_since_completion = 0.5 * seconds_elapsed;
            self.last_completed_chord = self.current_chord.clone();
            self.seconds_since_completion = 0.5 * seconds_elapsed;
        }
        else
        {
            self.seconds_since_completion += self.seconds_since_completion;
        }

        // update current chord data
        self.current_chord = chord;
        self.seconds_of_current_chord = 0.5 * seconds_elapsed;

        // maybe emit completed chord
        if self.current_chord.is_released()
            && estimated_seconds_since_completion < self.seconds_to_forget_completed
        {
            self.seconds_since_completion = f32::INFINITY; // aka "set last completed chord to None"
            return self.create_report(Some(self.last_completed_chord.clone()));
        }

        self.create_report(None)
    }
    fn create_report(self: &Self, completed_chord: Option<TChord>) -> ChordMemoryReport<TChord>
    {
        ChordMemoryReport {
            completed_chord,
            current_chord_past_completion_time:
                self.seconds_of_current_chord >= self.seconds_to_complete,
        }
    }
}
pub struct ChordMemoryReport<TChord> {
    pub completed_chord: Option<TChord>,
    pub current_chord_past_completion_time: bool,
}

#[derive(Clone, Copy)]
pub enum ChordSide { Left, Right }

/// Represents a one hand chord of fingers curled on the valve index 
/// controllers - ignoring thumbs.
#[derive(Default, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Debug, Hash)]
pub struct Chord {
    pub bits: u8,
}
impl Chord {
    pub const ALL_PRESSED: Chord = Chord::new(true, true, true, true);
    pub const NONE_PRESSED: Chord = Chord::new(false, false, false, false);

    pub const fn new(index: bool, middle: bool, ring: bool, pinkie: bool) -> Self {
        Self {
            bits: 1 * (index as u8)
                + 2 * (middle as u8)
                + 4 * (ring as u8)
                + 8 * (pinkie as u8),
        }
    }
    pub const fn index(self: &Self)  -> bool { self.bits & 1 > 0 }
    pub const fn middle(self: &Self) -> bool { self.bits & 2 > 0 }
    pub const fn ring(self: &Self)   -> bool { self.bits & 4 > 0 }
    pub const fn pinkie(self: &Self) -> bool { self.bits & 8 > 0 }

// TODO this should be a const array
    pub fn all_chords() -> [Chord; 16] {
        let all_chords_iterator = (0..16).into_iter()
            .map(|u| Chord{ bits: u });
        let ret: [Chord; 16] = all_chords_iterator
            .collect::<Vec<Chord>>().try_into().unwrap();
        ret
    }

    pub fn display(self: &Self, side: ChordSide, bold: bool) -> [char; 4]
    {
        let c = self;
        let (up, down) = if bold {('I', 'o')} else {('i', '.')};
        let v = |is_down: bool| if is_down {down} else {up};
        match side {
            ChordSide::Left =>
                [ v(c.pinkie()), v(c.ring()), v(c.middle()), v(c.index()) ],
            ChordSide::Right =>
                [ v(c.index()), v(c.middle()), v(c.ring()), v(c.pinkie()) ],
        }
    }
}

/// Represents a chord of both hands.
#[derive(Default, Clone, Copy, PartialEq, PartialOrd, Eq)]
pub struct FullChord {
    pub left_chord: Chord,
    pub right_chord: Chord,
}
impl ChordWithRelease for FullChord {
    fn is_released(self: &Self) -> bool { self.right_chord == Chord::NONE_PRESSED }
}
impl FullChord {
    pub fn side(self: Self, side: ChordSide) -> Chord {
        match side {
            ChordSide::Left => self.left_chord,
            ChordSide::Right => self.right_chord,
        }
    }
}

/// A list of (optional) order numbers for all "single hand" chords.
/// The reasonable order is in binary interpretation with index for 1 or 0,
/// ..., pinkie for 8 or 0 - but skipping unreasonable chords.
/// Unreasonable chords get None for order.
/// Unreasonable chords are:
///     * all fingers down - has special meaning as "this does nothing".
///     * all fingers up - has special meaning as "fully released".
///     * all chords with ring finger up but pinkied and middle finger down
///         - these are really awkward to do with valve index controllers.
pub fn get_good_default_chord_orders() -> Vec<Option<u8>>
{
    let mut next_order: u8 = 0;
    let right_chord_orders: Vec<Option<u8>> = Chord::all_chords().iter()
        .map(|c| {
            if *c == Chord::ALL_PRESSED || *c == Chord::NONE_PRESSED
            {
                None
            }
            else if c.middle() && (!c.ring()) && c.pinkie()
            {        // ring finger straight is really awkward 
                None // with pinkie and middle fingers curled
            }
            else
            {
                next_order += 1;
                Some(next_order - 1)
            }
        })
        .collect();
    right_chord_orders
}

